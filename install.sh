# This script creates queenhill default server on Ubuntu Server 18.04


    # Default Username Password

        password=$(openssl rand -base64 9)


    # Parameters + Clear
            
        publicip=$(dig +short myip.opendns.com @resolver1.opendns.com)
        
        clear
        echo " &"
        echo " └─· Installing queenhill server manager "


    # I am root ?

        if [ "x$(id -u)" != 'x0' ]; then
                echo "   └─ Error: this script can only be executed by root."
                exit 1
        fi


    # Set Hostname

        read -p "   ├──· Server Name (servername) → " hostname
        echo "   │  └── Hostname set as $hostname "


    # Set Hostname

        read -p "   ├──· Domain Name (domain.tld) → " domain
        echo "   │  └── Domain set as $domain "


    # Check dependencies

        echo "   ├──· Checking dependencies ... "
        apt update > /home/queen/logs/installation_log 2>&1
        echo "    │  └── All dependencies installed"


    # Create folders

        echo "   ├── Creating home folders ... "
        mkdir /home/queen
        mkdir /home/workerant
        mkdir /ant

        echo "   ├── Creating ~/logs folder ... "
        mkdir /home/queen/logs
        touch /home/queen/logs/installation_log

        echo "   ├── Creating ~/.ssh folder ... "
        mkdir /home/queen/.ssh
        touch /home/queen/.ssh/authorized_keys
        touch /home/queen/.ssh/id_rsa
        mkdir /home/workerant/.ssh
        touch /home/workerant/.ssh/authorized_keys
        touch /home/workerant/.ssh/id_rsa


    # Copy default user files (.profile, .bashrc) to user home folder

        echo "   ├──· Creating user default files ... "
        cp ./library/resources/.bashrc ./library/resources/.profile /home/queen/
        cp ./library/resources/.bashrc ./library/resources/.profile /home/workerant/


    # Edit users files with previous configs

        # Change default_editor on .bashrc

            echo "   │  ├── Setting default_editor=nano in ~/.bashrc"
            sed -i "s/default_editor/nano/g" "/home/queen/.bashrc"
            sed -i "s/default_editor/nano/g" "/home/workerant/.bashrc"

        # Change promt_color on .bashrc

            echo "   │  ├── Setting promt_color=yes in ~/.bashrc"
            sed -i "s/promt_color/no/g" "/home/queen/.bashrc"
            sed -i "s/promt_color/yes/g" "/home/workerant/.bashrc"

        # Change promt_color on .bashrc

            echo "   │  └── Setting promt_clock=no in ~/.bashrc"
            sed -i "s/promt_clock/no/g" "/home/queen/.bashrc"
            sed -i "s/promt_clock/yes/g" "/home/workerant/.bashrc"


    # Create users

            echo "   ├──· Creating user ... "
            useradd -s /bin/bash -d /home/queen queen
            useradd -s /bin/bash -d /home/workerant -p $password ant

            # Set root privilegies

                echo "    │  └── Creating admin user file on /etc/sudoers.d/ ..."
                touch /etc/sudoers.d/queen
                printf "%s\n" "# queen don't need password to use sudo as root" "queen ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/queen
                

            # Set Keys

                read -p "   ├── Admin SSH Public Key (openssh) → " admin_authorized_key
                printf "$admin_authorized_key" > /home/queen/.ssh/authorized_keys

                read -p "   ├── Admin SSH Private Key (openssh) → " admin_id_rsa
                printf "$admin_id_rsa" > /home/workerant/.ssh/id_rsa

                read -p "   ├── Worker SSH Public Key (openssh) → " ant_authorized_key
                printf "$ant_authorized_key" > /home/workerant/.ssh/authorized_keys

                read -p "   ├── Worker SSH Private Key (openssh) → " ant_id_rsa
                printf "$ant_id_rsa" > /home/workerant/.ssh/id_rsa


    # Set user permissions 

            echo "   ├── Setting user permissions ... "
            chown -R queen:queen /home/queen
            chmod 775 -R /home/queen
            chmod 644 /home/queen/.bashrc
            chmod 644 /home/queen/.profile
            chmod 744 -R /home/queen/.ssh/
            chmod 600 /home/queen/.ssh/id_rsa

            chown -R ant:ant /home/workerant
            chmod 775 -R /home/workerant
            chmod 644 /home/workerant/.bashrc
            chmod 644 /home/workerant/.profile
            chmod 744 -R /home/workerant/.ssh/
            chmod 600 /home/workerant/.ssh/id_rsa

            chown -R queen:ant /ant
            chmod 775 -R /ant


    # End message

            echo "   ├─· Installation finished. "
            echo "   │ ├── Login at https://$publicip:52000"
            echo "   │ └── User: ant | Password: $password"
            echo "   │"
            echo "   └── Visit us on https://queenhill.sh"
            echo " "
