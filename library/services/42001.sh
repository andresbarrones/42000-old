comand_selector () {
    
    if [ -z $command_selected ]; then
                                                            
        read -p "  » " command_selected

    fi

    case $command_selected in

        start )

            cd /ant/42001/
            pm2 start ./antton_bot.js
            ;;

        stop )

            cd /ant/42001/
            pm2 stop /antton_bot.js
            ;;

        restart )

            pm2 restart antton_bot
            ;;

        delete )

            pm2 delete antton_bot
            ;;

        * )

            echo "  » Invalid command, please try again. "
            command_selected=""
            comand_selector

    esac

}

comand_selector